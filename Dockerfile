ARG BUILD_HOME=/build-home

## build container
FROM maven:3.8.3-eclipse-temurin-11 as build-image

ARG BUILD_HOME
ENV APP_HOME=$BUILD_HOME
WORKDIR $APP_HOME

COPY pom.xml $APP_HOME/
COPY src $APP_HOME/src

RUN mvn package


## run container
FROM eclipse-temurin:11-alpine

ARG BUILD_HOME
ARG CA_CERT
ENV APP_HOME=$BUILD_HOME
ENV JAVA_OPTS=""

RUN echo "$CA_CERT" > /ca-certificate.crt
RUN keytool -cacerts -storepass changeit -noprompt -trustcacerts -importcert -alias mongo -file /ca-certificate.crt
RUN rm /ca-certificate.crt

COPY --from=build-image $APP_HOME/target/bm-bot.jar app.jar

ENTRYPOINT java $JAVA_OPTS -jar app.jar