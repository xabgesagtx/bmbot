# BM Bot


[![pipeline status](https://gitlab.com/xabgesagtx/bmbot/badges/master/pipeline.svg)](https://gitlab.com/xabgesagtx/bmbot/commits/master) [![Telegram](http://trellobot.doomdns.org/telegrambadge.svg)](https://telegram.me/bm20bot)

This is a unofficial telegram bot for the events of bewegungsmelder.org. It uses the JSON API created for the [android application](https://github.com/arnef/bewegungsmelder-android).

The bot is built with the awesome Spring Boot 2 and runs on Java 11.


## Configuration

All configuration can be done in the application.yml file.

```
config:
  botConfig:
    name: BOTNAME
    token: BOTTOKEN
spring:
  data:
    mongodb:
      username: MONGO_USERNAME
      password: MONGO_PASSWORD
      host: MONGO_HOST
      authentication-database: admin

```

## Build

Run maven to build the jar file

```
./mvnw clean package
```

Afterwards you have an executable jar file in `target/bm-bot.jar` that can also [function as an init-script](https://docs.spring.io/spring-boot/docs/current/reference/html/deployment-install.html).

## Requirements

* Java 11
* mongodb

## Development

For development purposes a docker-compose.yml is provided. Simply run:

```
docker-compose up -d
```

A mongodb database will be a available at port 27017. 

Afterwards you can start your bot via IDE if you set the configuration for the telegram bot properly (name and taken).

