package org.xabgesagtx.bm.bot.commands.inline;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.xabgesagtx.bm.bot.common.DateUtils;
import org.xabgesagtx.bm.bot.model.ArticleFactory;
import org.xabgesagtx.bm.bot.model.responses.InlineArticle;
import org.xabgesagtx.bm.events.persistence.model.Event;
import org.xabgesagtx.bm.events.service.EventService;

import java.time.LocalDate;
import java.util.Collections;
import java.util.Optional;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class DateInlineCommandTest {

	@InjectMocks
	private DateInlineCommand command;

	@Mock
	private ArticleFactory articleFactory;

	@Mock
	private DateUtils dateUtils;

	@Mock
	private EventService facade;

	@Test
	public void toArticle_no_match() {
		var notADate = "not a date";
		when(dateUtils.getLocalDateFromString(notADate)).thenReturn(Optional.empty());

		var actual = command.toArticle(notADate);

		assertTrue(actual.isEmpty());
	}

	@Test
	public void toArticle_match_but_no_event() {
		var dateText = "a date";
		var date = LocalDate.now();
		var events = Collections.<Event>emptyList();

		when(dateUtils.getLocalDateFromString(dateText)).thenReturn(Optional.of(date));
		when(facade.getEventsForDate(date)).thenReturn(events);

		var actual = command.toArticle(dateText);

		assertTrue(actual.isEmpty());
	}

	@Test
	public void toArticle_match_with_events() {
		var dateText = "a date";
		var date = LocalDate.now();
		var event1 = Event.builder().id("1").build();
		var events = Collections.singletonList(event1);
		var article1 = InlineArticle.builder().id("1").build();

		when(dateUtils.getLocalDateFromString(dateText)).thenReturn(Optional.of(date));
		when(facade.getEventsForDate(date)).thenReturn(events);
		when(articleFactory.create(event1)).thenReturn(article1);

		var actual = command.toArticle(dateText);
		var expected = Collections.singletonList(article1);

		assertThat(actual, is(expected));
	}
}