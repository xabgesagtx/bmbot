package org.xabgesagtx.bm.bot.commands.chat;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.xabgesagtx.bm.bot.common.DateUtils;
import org.xabgesagtx.bm.bot.formatter.DaySummariesFormatter;
import org.xabgesagtx.bm.bot.model.responses.ChatMessageResponse;
import org.xabgesagtx.bm.bot.model.responses.KeyboardType;
import org.xabgesagtx.bm.events.persistence.model.Event;
import org.xabgesagtx.bm.events.service.EventService;

import java.time.LocalDate;
import java.util.Collections;
import java.util.Optional;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class DateChatCommandTest {

	@InjectMocks
	private DateChatCommand command;

	@Mock
	private DaySummariesFormatter formatter;

	@Mock
	private DateUtils dateUtils;

	@Mock
	private EventService facade;

	@Test
	public void toResponse_no_match() {
		var notADate = "not a date";
		when(dateUtils.getLocalDateFromString(notADate)).thenReturn(Optional.empty());

		var actual = command.toResponse(notADate);

		assertFalse(actual.isPresent());
	}

	@Test
	public void toResponse_match_but_no_event() {
		var dateText = "a date";
		var date = LocalDate.now();
		var events = Collections.<Event>emptyList();
		var formattedEvents = "formatted events";

		when(dateUtils.getLocalDateFromString(dateText)).thenReturn(Optional.of(date));
		when(facade.getEventsForDate(date)).thenReturn(events);
		when(formatter.format(date, events)).thenReturn(formattedEvents);

		var actual = command.toResponse(dateText);
		var expected = Optional.of(new ChatMessageResponse(formattedEvents, KeyboardType.MAIN));

		assertThat(actual, is(expected));
	}

	@Test
	public void toResponse_match_with_events() {
		var dateText = "a date";
		var date = LocalDate.now();
		var events = Collections.singletonList(Event.builder().id("1").build());
		var formattedEvents = "formatted events";

		when(dateUtils.getLocalDateFromString(dateText)).thenReturn(Optional.of(date));
		when(facade.getEventsForDate(date)).thenReturn(events);
		when(formatter.format(date, events)).thenReturn(formattedEvents);

		var actual = command.toResponse(dateText);
		var expected = Optional.of(new ChatMessageResponse(formattedEvents, KeyboardType.MAIN));

		assertThat(actual, is(expected));
	}

}