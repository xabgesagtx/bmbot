package org.xabgesagtx.bm.bot.common;


import org.junit.jupiter.api.Test;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Collections;
import java.util.Optional;
import java.util.concurrent.TimeUnit;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;


public class DateUtilsTest {

	private final DateUtils utils = new DateUtils();

	@Test
	public void testGetFullDate() {
		assertNull(utils.getFullDate(null, LocalDate.now()));
		assertNull(utils.getFullDate(10000L, null));
		assertEquals(LocalDateTime.of(2016, 1, 3, 3, 0), utils.getFullDate(TimeUnit.SECONDS.convert(3, TimeUnit.HOURS), LocalDate.of(2016, 1, 2)));
		assertEquals(LocalDateTime.of(2016, 1, 2, 4, 0), utils.getFullDate(TimeUnit.SECONDS.convert(4, TimeUnit.HOURS), LocalDate.of(2016, 1, 2)));
	}

	@Test
	public void testGetWeekend() {
		assertEquals(Arrays.asList(LocalDate.of(2016, 5, 20), LocalDate.of(2016, 5, 21), LocalDate.of(2016, 5, 22)),
				utils.getWeekend(LocalDate.of(2016, 5, 18)));
		assertEquals(Arrays.asList(LocalDate.of(2016, 5, 20), LocalDate.of(2016, 5, 21), LocalDate.of(2016, 5, 22)),
				utils.getWeekend(LocalDate.of(2016, 5, 20)));
		assertEquals(Arrays.asList(LocalDate.of(2016, 5, 21), LocalDate.of(2016, 5, 22)), utils.getWeekend(LocalDate.of(2016, 5, 21)));
		assertEquals(Collections.singletonList(LocalDate.of(2016, 5, 22)), utils.getWeekend(LocalDate.of(2016, 5, 22)));
		assertEquals(Arrays.asList(LocalDate.of(2016, 5, 27), LocalDate.of(2016, 5, 28), LocalDate.of(2016, 5, 29)),
				utils.getWeekend(LocalDate.of(2016, 5, 23)));
	}
	
	@Test
	public void testGetDayOfWeekFromString() {
		assertEquals(Optional.empty(),utils.getDayOfWeekFromString(""));
		assertEquals(Optional.of(DayOfWeek.MONDAY),utils.getDayOfWeekFromString("Monday"));
		assertEquals(Optional.of(DayOfWeek.MONDAY),utils.getDayOfWeekFromString("Montag"));
		assertEquals(Optional.of(DayOfWeek.MONDAY),utils.getDayOfWeekFromString("Mon"));
		assertEquals(Optional.of(DayOfWeek.TUESDAY),utils.getDayOfWeekFromString("Dienstag"));
		assertEquals(Optional.of(DayOfWeek.WEDNESDAY),utils.getDayOfWeekFromString("Mittwoch"));
		assertEquals(Optional.of(DayOfWeek.THURSDAY),utils.getDayOfWeekFromString("Donnerstag"));
		assertEquals(Optional.of(DayOfWeek.FRIDAY),utils.getDayOfWeekFromString("Freitag"));
		assertEquals(Optional.of(DayOfWeek.SATURDAY),utils.getDayOfWeekFromString("Samstag"));
		assertEquals(Optional.of(DayOfWeek.SUNDAY),utils.getDayOfWeekFromString("Sonntag"));
	}
	
	@Test
	public void testGetFittingDateForDayOfWeek() {
		assertEquals(LocalDate.of(2016,5,27), utils.getFittingDateForDayOfWeek(DayOfWeek.FRIDAY, LocalDate.of(2016,5,21)));
		assertEquals(LocalDate.of(2016,5,21), utils.getFittingDateForDayOfWeek(DayOfWeek.SATURDAY, LocalDate.of(2016,5,21)));
		assertEquals(LocalDate.of(2016,5,22), utils.getFittingDateForDayOfWeek(DayOfWeek.SUNDAY, LocalDate.of(2016,5,21)));
	}
	
	@Test
	public void testGetLocalDateFromString() {
		assertEquals(Optional.of(LocalDate.of(2016, 5, 27)), utils.getLocalDateFromString("27.5.",LocalDate.of(2016, 4, 20)));
		assertEquals(Optional.of(LocalDate.of(2016, 5, 27)), utils.getLocalDateFromString("27.5.",LocalDate.of(2016, 5, 28)));
		assertEquals(Optional.of(LocalDate.of(2017, 5, 27)), utils.getLocalDateFromString("27.5.",LocalDate.of(2016, 7, 28)));
		assertEquals(Optional.of(LocalDate.of(2016, 5, 27)), utils.getLocalDateFromString("27.5.2016",LocalDate.of(2016, 7, 28)));
		assertEquals(Optional.of(LocalDate.of(2016, 6, 27)), utils.getLocalDateFromString("27.Juni.2016",LocalDate.of(2016, 7, 28)));
		assertEquals(Optional.of(LocalDate.of(2016, 6, 27)), utils.getLocalDateFromString("27.June.2016",LocalDate.of(2016, 7, 28)));
	}

}
