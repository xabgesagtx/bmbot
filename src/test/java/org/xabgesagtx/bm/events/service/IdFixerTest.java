package org.xabgesagtx.bm.events.service;

import org.junit.jupiter.api.Test;
import org.xabgesagtx.bm.events.persistence.model.Event;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.assertEquals;


public class IdFixerTest {

	@Test
	public void test() {
		var input = Arrays.asList(Event.builder().id("1").build(),
				Event.builder().id("1").build(),
				Event.builder().id("2").build(),
				Event.builder().id("3").build(),
				Event.builder().id("3").build(),
				Event.builder().id("1").build());
		var expected = Arrays.asList(Event.builder().id("1A").build(),
				Event.builder().id("1B").build(),
				Event.builder().id("2").build(),
				Event.builder().id("3A").build(),
				Event.builder().id("3B").build(),
				Event.builder().id("1C").build());
		new IdFixer().fixIds(input);
		assertEquals(expected, input);
	}

}
