package org.xabgesagtx.bm.events.persistence.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown=true)
@Document
public class Event {

	@Id
	private String id;
	
    private LocalDate date;

    private LocalDateTime fullStartDate;

    private String title;
    
    private String description;
    
    private List<Band> bands;
    
    private Location location;
    
	private List<Link> links;
    
    private String price;
    
    private String moreInfos;
    
	private List<String> type;

    private boolean canceled;
    
    private boolean favorite;

	public String getLocationTitle() {
		return getLocation() != null ? getLocation().getTitle() : StringUtils.EMPTY;
	}
    
}
