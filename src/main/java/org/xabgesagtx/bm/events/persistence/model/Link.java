package org.xabgesagtx.bm.events.persistence.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.StringUtils;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@JsonIgnoreProperties(ignoreUnknown=true)
public class Link {
	
	private String url;
	private String title;
	
	public boolean isValid() {
		return StringUtils.isNotBlank(title) && StringUtils.isNotBlank(url);
	}

}
