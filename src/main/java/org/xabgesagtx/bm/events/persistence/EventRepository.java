package org.xabgesagtx.bm.events.persistence;

import org.springframework.data.repository.CrudRepository;
import org.xabgesagtx.bm.events.persistence.model.Event;

import java.time.LocalDate;
import java.util.List;

public interface EventRepository extends CrudRepository<Event, String> {

	List<Event> findByDateOrderByFullStartDate(LocalDate date);

}
