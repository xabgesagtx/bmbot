package org.xabgesagtx.bm.events.rest;

import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.xabgesagtx.bm.events.persistence.EventRepository;
import org.xabgesagtx.bm.events.persistence.model.Event;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/rest_debug")
@RequiredArgsConstructor
public class DebugController {
	
	private final EventRepository repo;
	
	@RequestMapping(value = "events", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<Event> getAllEvents() {
		var result = new ArrayList<Event>();
		for (Event event : repo.findAll()) {
			result.add(event);
		}
		return result;
	}

}
