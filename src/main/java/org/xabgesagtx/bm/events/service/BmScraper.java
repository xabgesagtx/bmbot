package org.xabgesagtx.bm.events.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.xabgesagtx.bm.api.BmApiClient;
import org.xabgesagtx.bm.api.model.BandApiDTO;
import org.xabgesagtx.bm.api.model.EventApiDTO;
import org.xabgesagtx.bm.api.model.LinkApiDTO;
import org.xabgesagtx.bm.api.model.LocationApiDTO;
import org.xabgesagtx.bm.bot.common.DateUtils;
import org.xabgesagtx.bm.events.persistence.model.Band;
import org.xabgesagtx.bm.events.persistence.model.Event;
import org.xabgesagtx.bm.events.persistence.model.Link;
import org.xabgesagtx.bm.events.persistence.model.Location;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Component
@RequiredArgsConstructor
@Slf4j
public class BmScraper {
	private final BmApiClient apiClient;
	private final IdFixer idFixer;
	private final DateUtils dateUtils;

	public List<Event> getEvents() {
		try {
			var events = apiClient.getEvents().stream()
					.map(this::convert)
					.collect(Collectors.toList());
			idFixer.fixIds(events);
			return events;
		} catch (Exception e) {
			log.error("Failed to update events due to exception", e);
			return Collections.emptyList();
		}
	}

	private Event convert(EventApiDTO dto) {
		return Event.builder()
				.id(dto.getId().toString())
				.canceled(dto.isCanceled())
				.favorite(dto.isFavorite())
				.date(dto.getDate())
				.fullStartDate(dateUtils.getFullDate(dto.getTimeStart(), dto.getDate()))
				.description(dto.getDescription())
				.moreInfos(dto.getMoreInfos())
				.title(dto.getTitle())
				.price(dto.getPrice())
				.type(dto.getType())
				.bands(dto.getBands().stream().map(this::convert).collect(Collectors.toList()))
				.links(dto.getLinks().stream().map(this::convert).collect(Collectors.toList()))
				.location(convert(dto.getLocation()))
				.build();
	}

	private Link convert(LinkApiDTO dto) {
		return Link.builder()
				.title(dto.getTitle())
				.url(dto.getUrl())
				.build();
	}

	private Location convert(LocationApiDTO dto) {
		if (dto == null) {
			return null;
		} else {
			return Location.builder()
					.title(dto.getTitle())
					.category(dto.getCategory())
					.description(dto.getDescription())
					.links(dto.getLinks().stream().map(this::convert).collect(Collectors.toList()))
					.build();
		}
	}

	private Band convert(BandApiDTO dto) {
		return Band.builder()
				.title(dto.getTitle())
				.description(dto.getDescription())
				.links(dto.getLinks().stream().map(this::convert).collect(Collectors.toList()))
				.build();
	}

}
