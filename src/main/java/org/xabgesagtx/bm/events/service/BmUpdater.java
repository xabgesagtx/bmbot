package org.xabgesagtx.bm.events.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component
@Slf4j
@RequiredArgsConstructor
public class BmUpdater {
	
	private final EventService facade;
	private final BmScraper scraper;
	
	@Scheduled(cron = "0 0 0/1 * * ?")
	public void update() {
		try {
			log.info("Running update of events");
			var events = scraper.getEvents();
			log.info("Found {} events", events.size());
			if (!events.isEmpty()) {
				facade.replaceAll(events);
			}
		} catch (Exception e) {
			log.error("Failed to update events due to exception", e);
		}
	}
	
	@PostConstruct
	public void startUp() {
		update();
	}

}
