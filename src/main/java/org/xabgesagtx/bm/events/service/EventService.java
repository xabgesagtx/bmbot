package org.xabgesagtx.bm.events.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.xabgesagtx.bm.bot.common.DateUtils;
import org.xabgesagtx.bm.events.persistence.model.Event;
import org.xabgesagtx.bm.events.persistence.EventRepository;

import java.time.LocalDate;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Slf4j
@RequiredArgsConstructor
public class EventService {
	
	private final EventRepository repo;
	private final DateUtils dateUtils;
	
	public List<Event> getEventsForToday() {
		return getEventsForDate(dateUtils.getToday());
	}
	
	public List<Event> getEventsForTomorrow() {
		return getEventsForDate(dateUtils.getTomorrow());
	}
	
	public List<Event> getEventsForDate(LocalDate date) {
		return repo.findByDateOrderByFullStartDate(date);
	}
	
	public Optional<Event> getEvent(String cleanId) {
		return repo.findById(cleanId);
	}

	public Map<LocalDate, List<Event>> getEventsForDates(List<LocalDate> dates) {
		return dates.stream()
				.map(date -> Pair.of(date, getEventsForDate(date)))
				.collect(Collectors.toMap(Pair::getLeft, Pair::getRight));
	}

	@Transactional
	public void replaceAll(List<Event> events) {
		repo.deleteAll();
		repo.saveAll(events);
	}
}
