package org.xabgesagtx.bm.events.service;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.xabgesagtx.bm.events.persistence.model.Event;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.IntStream;

import static java.util.stream.Collectors.*;

@Component
public class IdFixer {
	
	@RequiredArgsConstructor
	class IdProvider {
		@NonNull
		private final Map<String,Long> occurences;
		private final List<String> providedIds = new ArrayList<>();
		private final List<String> alphabet = IntStream.rangeClosed('A', 'Z').mapToObj(c -> String.valueOf((char) c)).collect(toList());
		
		void setIdFor(Event event) {
			String id;
			if (occurences.get(event.getId()) > 1) {
				id = getFixedIdFor(event.getId());
			} else {
				id = event.getId();
			}
			providedIds.add(id);
			event.setId(id);
		}
		
		String getFixedIdFor(String id) {
			return alphabet.stream()
					.map(letter -> id + letter)
					.filter(cleanedId -> !providedIds.contains(cleanedId))
					.findFirst()
					.orElseGet(() -> getFixedIdFor(id + alphabet.get(0)));
		}
		
		
	}
	
	void fixIds(List<Event> events) {
		Map<String, Long> occurences = events.stream().collect(groupingBy(Event::getId, counting()));
		IdProvider idProvider = new IdProvider(occurences);
		events.forEach(idProvider::setIdFor);
				
	}

}
