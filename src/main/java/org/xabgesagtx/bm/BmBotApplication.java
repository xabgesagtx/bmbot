package org.xabgesagtx.bm;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class BmBotApplication {

	public static void main(String[] args) {
		SpringApplication.run(BmBotApplication.class, args);
	}
}
