package org.xabgesagtx.bm.api.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@JsonIgnoreProperties(ignoreUnknown=true)
public class LocationApiDTO {
	
	private String category;
	private String title;
	
	private String description;
	
	@Builder.Default private List<LinkApiDTO> links = new ArrayList<>();
	
	public void setLinks(List<LinkApiDTO> links) {
		if (links != null) {
			this.links = links;
		}
	}
	

}
