package org.xabgesagtx.bm.api.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class EventApiDTO {

	private Integer id;

	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
	private LocalDate date;

	private String title;

	private String description;

	@Builder.Default
	private List<BandApiDTO> bands = new ArrayList<>();

	public void setBands(List<BandApiDTO> bands) {
		if (bands != null) {
			this.bands = bands;
		}
	}

	private LocationApiDTO location;

	@Builder.Default
	private List<LinkApiDTO> links = new ArrayList<>();

	public void setLinks(List<LinkApiDTO> links) {
		if (links != null) {
			this.links = links;
		}
	}

	private Long timeEntry;

	private Long timeStart;

	private String price;

	@JsonProperty("more_infos")
	private String moreInfos;

	@Builder.Default
	private List<String> type = new ArrayList<>();

	public void setType(List<String> type) {
		if (type != null) {
			this.type = type;
		}
	}

	private boolean canceled;

	private boolean favorite;

}
