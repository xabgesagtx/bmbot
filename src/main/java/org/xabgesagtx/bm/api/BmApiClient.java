package org.xabgesagtx.bm.api;

import io.github.resilience4j.retry.annotation.Retry;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.xabgesagtx.bm.api.model.EventApiDTO;
import org.xabgesagtx.bm.config.BmProperties;

import java.util.Collections;
import java.util.List;

@Component
@RequiredArgsConstructor
@Slf4j
public class BmApiClient {

	private final BmProperties props;
	private final RestTemplate restTemplate;

	@Retry(name = "events")
	public List<EventApiDTO> getEvents() {
		log.info("Downloading events from bm api");
		var result = Collections.<EventApiDTO>emptyList();
		var responseEntity = restTemplate.exchange(props.url, HttpMethod.GET, null, new ParameterizedTypeReference<List<EventApiDTO>>() {});
		if (responseEntity.hasBody()) {
			result = responseEntity.getBody();
		}
		return result;
	}
}
