package org.xabgesagtx.bm.spring;

import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

import java.time.Duration;

/**
 * Configuration for rest template to scrape events
 */
@Configuration
public class RestTemplateConfig {
	
	@Bean
	public RestTemplate restTemplate(RestTemplateBuilder builder) {
		return builder
				// we don't care if this takes too long, we just want it to return at some point
				.setReadTimeout(Duration.ofMinutes(2))
				.setConnectTimeout(Duration.ofMinutes(2))
				.build();
	}
	
	
}
