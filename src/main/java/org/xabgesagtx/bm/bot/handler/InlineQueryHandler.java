package org.xabgesagtx.bm.bot.handler;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.telegram.telegrambots.meta.api.methods.AnswerInlineQuery;
import org.telegram.telegrambots.meta.api.methods.ParseMode;
import org.telegram.telegrambots.meta.api.objects.inlinequery.InlineQuery;
import org.telegram.telegrambots.meta.api.objects.inlinequery.inputmessagecontent.InputTextMessageContent;
import org.telegram.telegrambots.meta.api.objects.inlinequery.result.InlineQueryResult;
import org.telegram.telegrambots.meta.api.objects.inlinequery.result.InlineQueryResultArticle;
import org.xabgesagtx.bm.bot.commands.inline.IInlineCommand;
import org.xabgesagtx.bm.bot.model.responses.InlineArticle;
import org.xabgesagtx.bm.config.BotProperties;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Component
@RequiredArgsConstructor
@Slf4j
public class InlineQueryHandler {

	private final BotProperties properties;
	private final List<IInlineCommand> commands;

	@Transactional
	public AnswerInlineQuery handle(InlineQuery inlineQuery) {
		var text = getText(inlineQuery);
		var id = getId(inlineQuery);
		log.info("Got inline query: {} from {}", text, id);
		var results = commands.stream().map(command -> command.toArticle(text))
				.filter(list -> !list.isEmpty())
				.findFirst()
				.map(this::toQueryResults)
				.orElseGet(Collections::emptyList);
		log.info("Returning {} entries", results.size());
		var answer = new AnswerInlineQuery();
		answer.setInlineQueryId(id);
		answer.setResults(results);
		answer.setCacheTime(properties.inlineCacheTime);
		return answer;
	}

	private List<InlineQueryResult> toQueryResults(List<InlineArticle> articles) {
		return articles.stream()
				.map(this::toQueryResult)
				.collect(Collectors.toList());
	}

	private InlineQueryResult toQueryResult(InlineArticle article) {
		var result = new InlineQueryResultArticle();
		result.setTitle(article.getTitle());
		result.setId(article.getId());
		result.setDescription(article.getDescription());
		var content = new InputTextMessageContent();
		content.setParseMode(ParseMode.HTML);
		content.setMessageText(article.getMessageText());
		result.setInputMessageContent(content);
		return result;
	}

	private String getId(InlineQuery inlineQuery) {
		return inlineQuery.getId();
	}

	private String getText(InlineQuery inlineQuery) {
		return StringUtils.trimToEmpty(inlineQuery.getQuery());
	}
}
