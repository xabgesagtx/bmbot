package org.xabgesagtx.bm.bot.handler;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboard;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.KeyboardButton;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.KeyboardRow;
import org.xabgesagtx.bm.bot.commands.chat.IChatCommand;
import org.xabgesagtx.bm.bot.common.CommandConstants;
import org.xabgesagtx.bm.bot.model.responses.ChatMessageResponse;

import java.time.DayOfWeek;
import java.time.format.TextStyle;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.Optional;

@Component
@RequiredArgsConstructor
@Slf4j
public class ChatMessageHandler {

	private final List<IChatCommand> commands;

	@Transactional
	public SendMessage handle(Message incoming) {
		var text = getText(incoming);
		var chatId = getChatId(incoming);
		var responseOpt = commands.stream()
				.map(command -> command.toResponse(text))
				.flatMap(Optional::stream)
				.findFirst();
		return responseOpt.map(response -> createMessage(response, chatId)).orElseGet(() -> {
					log.info("Failed to create response for {}", text);
					return getNoResultMessage(chatId);
				});

	}

	private SendMessage createMessage(ChatMessageResponse response, String chatId) {
		var message = new SendMessage();
		message.setChatId(chatId);
		message.enableHtml(true);
		message.setText(response.getText());
		switch (response.getKeyboardType()) {
			case MAIN:
				message.setReplyMarkup(createBasicKeyboard());
				break;
			case WEEKDAY:
				message.setReplyMarkup(createWeekdayKeyboard());
		}
		return message;
	}

	private ReplyKeyboard createWeekdayKeyboard() {
		var keyboardMarkup = new ReplyKeyboardMarkup();
		keyboardMarkup.setResizeKeyboard(true);
		var firstRow = new KeyboardRow();
		firstRow.add(getDisplayName(DayOfWeek.MONDAY));
		firstRow.add(getDisplayName(DayOfWeek.TUESDAY));
		firstRow.add(getDisplayName(DayOfWeek.WEDNESDAY));
		var secondRow = new KeyboardRow();
		secondRow.add(getDisplayName(DayOfWeek.THURSDAY));
		secondRow.add(getDisplayName(DayOfWeek.FRIDAY));
		secondRow.add(getDisplayName(DayOfWeek.SATURDAY));
		var thirdRow = new KeyboardRow();
		thirdRow.add(getDisplayName(DayOfWeek.SUNDAY));
		thirdRow.add(CommandConstants.BACK_TEXT);
		keyboardMarkup.setKeyboard(Arrays.asList(firstRow, secondRow, thirdRow));
		return keyboardMarkup;
	}

	private String getDisplayName(DayOfWeek dayOfWeek) {
		return dayOfWeek.getDisplayName(TextStyle.FULL, Locale.GERMAN);
	}

	private ReplyKeyboardMarkup createBasicKeyboard() {
		log.debug("Setting basic keyboard");
		var keyboardMarkup = new ReplyKeyboardMarkup();
		keyboardMarkup.setResizeKeyboard(true);
		var todayButton = new KeyboardButton("Heute");
		var tomorrowButton = new KeyboardButton("Morgen");
		var firstRow = new KeyboardRow();
		firstRow.add(todayButton);
		firstRow.add(tomorrowButton);
		var secondRow = new KeyboardRow();
		var weekendButton = new KeyboardButton("Wochenende");
		secondRow.add(weekendButton);
		secondRow.add(CommandConstants.WEEKDAYS_TEXT);
		keyboardMarkup.setKeyboard(List.of(firstRow, secondRow));
		return keyboardMarkup;
	}

	private SendMessage getNoResultMessage(String chatId) {
		SendMessage response = new SendMessage();
		response.setChatId(chatId);
		response.setText("Hmm, ich weiß gerade nicht, was du willst. Probier es nochma!");
		return response;
	}

	private String getChatId(Message incoming) {
		return incoming.getChatId().toString();
	}

	private String getText(Message incoming) {
		return StringUtils.trimToEmpty(incoming.getText());
	}
}
