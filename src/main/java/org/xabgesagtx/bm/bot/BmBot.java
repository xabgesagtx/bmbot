package org.xabgesagtx.bm.bot;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.methods.BotApiMethod;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import org.xabgesagtx.bm.bot.handler.ChatMessageHandler;
import org.xabgesagtx.bm.bot.handler.InlineQueryHandler;
import org.xabgesagtx.bm.config.BotProperties;

@Component
@Slf4j
@RequiredArgsConstructor
public class BmBot extends TelegramLongPollingBot {
	
	private final BotProperties properties;
	private final ChatMessageHandler chatMessageHandler;
	private final InlineQueryHandler inlineQueryHandler;

	@Override
	public String getBotUsername() {
		return properties.name;
	}

	@Override
	public void onUpdateReceived(Update update) {
		if (update.hasMessage()) {
			var response = chatMessageHandler.handle(update.getMessage());
			sendWithExceptionHandling(response);
		} else if (update.hasInlineQuery()) {
			var response = inlineQueryHandler.handle(update.getInlineQuery());
			sendWithExceptionHandling(response);
		}
	}

	@Override
	public String getBotToken() {
		return properties.token;
	}

	private void sendWithExceptionHandling(BotApiMethod<?> apiMethod) {
		try {
			sendApiMethod(apiMethod);
		} catch (TelegramApiException e) {
			log.error("Error sending response \"{}\"", apiMethod, e);
		}
	}

}
