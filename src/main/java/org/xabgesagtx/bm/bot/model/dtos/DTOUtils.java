package org.xabgesagtx.bm.bot.model.dtos;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.StringUtils;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
class DTOUtils {

	static String escape(String text) {
		return replaceWithHTMLEntities(text);
	}

	private static String replaceWithHTMLEntities(String text) {
		return StringUtils.defaultString(text).replace("&", "&amp;").replace("<", "&lt;").replace(">", "&gt;");
	}

}
