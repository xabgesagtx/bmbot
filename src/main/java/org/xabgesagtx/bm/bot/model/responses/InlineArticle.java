package org.xabgesagtx.bm.bot.model.responses;

import lombok.Builder;
import lombok.Value;

@Value
@Builder
public class InlineArticle {

	private String id;
	private String title;
	private String description;
	private String messageText;

}
