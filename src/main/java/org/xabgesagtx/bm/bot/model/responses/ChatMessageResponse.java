package org.xabgesagtx.bm.bot.model.responses;

import lombok.Value;

@Value
public class ChatMessageResponse {

	private final String text;
	private final KeyboardType keyboardType;

}
