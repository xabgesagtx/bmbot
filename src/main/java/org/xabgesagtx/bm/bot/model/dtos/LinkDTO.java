package org.xabgesagtx.bm.bot.model.dtos;

import lombok.Builder;
import lombok.Value;
import org.xabgesagtx.bm.events.persistence.model.Link;

import static org.xabgesagtx.bm.bot.model.dtos.DTOUtils.escape;

@Value
@Builder
public class LinkDTO {

	String title;
	String url;

	static LinkDTO fromLink(Link link) {
		return builder()
				.title(escape(link.getTitle()))
				.url(link.getUrl())
				.build();
	}
}
