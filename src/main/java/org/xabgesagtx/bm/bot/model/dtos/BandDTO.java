package org.xabgesagtx.bm.bot.model.dtos;

import lombok.Builder;
import lombok.Value;
import org.xabgesagtx.bm.events.persistence.model.Band;
import org.xabgesagtx.bm.events.persistence.model.Link;

import java.util.List;
import java.util.stream.Collectors;

import static org.xabgesagtx.bm.bot.model.dtos.DTOUtils.escape;

@Value
@Builder
public class BandDTO {

	private String title;
	private String description;
	private List<LinkDTO> links;

	static BandDTO fromBand(Band band) {
		List<LinkDTO> links = band.getLinks().stream()
				.filter(Link::isValid)
				.map(LinkDTO::fromLink)
				.collect(Collectors.toList());
		return builder()
				.title(escape(band.getTitle()))
				.description(escape(band.getDescription()))
				.links(links)
				.build();
	}

}
