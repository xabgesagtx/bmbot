package org.xabgesagtx.bm.bot.model.dtos;

import lombok.Builder;
import lombok.Value;
import org.apache.commons.lang3.StringUtils;
import org.xabgesagtx.bm.events.persistence.model.Event;
import org.xabgesagtx.bm.events.persistence.model.Link;

import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.stream.Collectors;

import static org.xabgesagtx.bm.bot.common.CommandConstants.DATE_FORMATTER;
import static org.xabgesagtx.bm.bot.model.dtos.DTOUtils.escape;

@Value
@Builder
public class EventDetailsDTO {

	String cleanId;
	String title;
	String date;
	String startTime;
	String locationTitle;
	String price;
	String description;
	List<LinkDTO> links;
	List<BandDTO> bands;
	boolean canceled;

	public static EventDetailsDTO fromEvent(Event event) {
		List<BandDTO> bands = event.getBands().stream()
				.map(BandDTO::fromBand)
				.collect(Collectors.toList());
		List<LinkDTO> links = event.getLinks().stream()
				.filter(Link::isValid)
				.map(LinkDTO::fromLink)
				.collect(Collectors.toList());
		return builder()
				.cleanId(event.getId())
				.title(escape(event.getTitle()))
				.description(escape(event.getDescription()))
				.locationTitle(escape(event.getLocationTitle()))
				.price(escape(event.getPrice()))
				.date(getDateString(event))
				.startTime(escape(getStartTimeString(event)))
				.bands(bands)
				.links(links)
				.canceled(event.isCanceled())
				.build();
	}

	private static String getDateString(Event event) {
		return DATE_FORMATTER.format(event.getDate());
	}

	private static String getStartTimeString(Event event) {
		if (event.getFullStartDate() != null) {
			return DateTimeFormatter.ofPattern("H:mm").format(event.getFullStartDate());
		} else {
			return StringUtils.EMPTY;
		}
	}


}
