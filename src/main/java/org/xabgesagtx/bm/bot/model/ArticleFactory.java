package org.xabgesagtx.bm.bot.model;

import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;
import org.xabgesagtx.bm.bot.formatter.EventDetailsFormatter;
import org.xabgesagtx.bm.bot.model.responses.InlineArticle;
import org.xabgesagtx.bm.events.persistence.model.Event;

@Component
@RequiredArgsConstructor
public class ArticleFactory {

	private final EventDetailsFormatter formatter;

	public InlineArticle create(Event event) {
		var messageText = formatter.format(event);
		var description = StringUtils.isBlank(event.getDescription()) ? event.getLocationTitle() : event.getLocationTitle() + " - " + event.getDescription();
		return InlineArticle.builder()
				.id(event.getId())
				.title(event.getTitle())
				.description(description)
				.messageText(messageText)
				.build();
	}
}
