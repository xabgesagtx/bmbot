package org.xabgesagtx.bm.bot.commands.chat;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.xabgesagtx.bm.bot.common.DateUtils;
import org.xabgesagtx.bm.bot.formatter.DaySummariesFormatter;
import org.xabgesagtx.bm.bot.model.responses.ChatMessageResponse;
import org.xabgesagtx.bm.bot.model.responses.KeyboardType;
import org.xabgesagtx.bm.events.service.EventService;

import java.time.LocalDate;
import java.util.Optional;

@Component
@RequiredArgsConstructor
public class WeekdayChatCommand implements IChatCommand {

	private final EventService facade;
	private final DaySummariesFormatter formatter;
	private final DateUtils dateUtils;

	private String createMessageText(LocalDate date) {
		return formatter.format(date, facade.getEventsForDate(date));
	}

	@Override
	public Optional<ChatMessageResponse> toResponse(String text) {
		return dateUtils.getLocalDateFromDayOfWeekString(text)
				.map(this::createMessageText)
				.map(messageText -> new ChatMessageResponse(messageText, KeyboardType.MAIN));
	}
}
