package org.xabgesagtx.bm.bot.commands.chat;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;
import org.xabgesagtx.bm.bot.common.CommandConstants;
import org.xabgesagtx.bm.bot.model.responses.ChatMessageResponse;
import org.xabgesagtx.bm.bot.model.responses.KeyboardType;

@Component
public class WeekdayKeyboardChatCommand implements IMatchingChatCommand {

	private static final String MESSAGE = "Wähle einen Wochentag";

	@Override
	public boolean matches(String text) {
		return StringUtils.equals(text, CommandConstants.WEEKDAYS_TEXT);
	}

	@Override
	public ChatMessageResponse createResponse() {
		return new ChatMessageResponse(MESSAGE, KeyboardType.WEEKDAY);
	}

}
