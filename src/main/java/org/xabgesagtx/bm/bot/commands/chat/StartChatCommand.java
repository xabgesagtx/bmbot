package org.xabgesagtx.bm.bot.commands.chat;

import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;
import org.xabgesagtx.bm.bot.common.CommandConstants;
import org.xabgesagtx.bm.bot.model.responses.ChatMessageResponse;
import org.xabgesagtx.bm.bot.model.responses.KeyboardType;
import org.xabgesagtx.bm.config.BotProperties;

@Component
@RequiredArgsConstructor
public class StartChatCommand implements IMatchingChatCommand {
	
	private final BotProperties properties;

	@Override
	public boolean matches(String text) {
		return StringUtils.equals(text, getPrefixedName());
	}

	private String getPrefixedName() {
		return CommandConstants.PREFIX + CommandConstants.START;
	}

	@Override
	public ChatMessageResponse createResponse() {
		return new ChatMessageResponse(properties.startMessage, KeyboardType.MAIN);
	}

}
