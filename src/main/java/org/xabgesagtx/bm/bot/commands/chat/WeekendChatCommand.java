package org.xabgesagtx.bm.bot.commands.chat;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.xabgesagtx.bm.bot.common.CommandConstants;
import org.xabgesagtx.bm.bot.common.DateUtils;
import org.xabgesagtx.bm.bot.formatter.DaySummariesFormatter;
import org.xabgesagtx.bm.bot.model.responses.ChatMessageResponse;
import org.xabgesagtx.bm.bot.model.responses.KeyboardType;
import org.xabgesagtx.bm.events.service.EventService;

@Component
@RequiredArgsConstructor
public class WeekendChatCommand implements IMatchingChatCommand {

	private final EventService facade;
	private final DaySummariesFormatter formatter;
	private final DateUtils dateUtils;

	@Override
	public boolean matches(String text) {
		return CommandConstants.WEEKEND_PATTERN.matcher(text).matches();
	}

	@Override
	public ChatMessageResponse createResponse() {
		var messageText = createMessageText();
		return new ChatMessageResponse(messageText, KeyboardType.MAIN);
	}

	private String createMessageText() {
		var eventsPerDay = facade.getEventsForDates(dateUtils.getWeekend());
		return formatter.format(eventsPerDay);
	}

}
