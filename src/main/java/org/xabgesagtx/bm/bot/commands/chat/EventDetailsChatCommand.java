package org.xabgesagtx.bm.bot.commands.chat;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.xabgesagtx.bm.bot.formatter.EventDetailsFormatter;
import org.xabgesagtx.bm.bot.model.responses.ChatMessageResponse;
import org.xabgesagtx.bm.bot.model.responses.KeyboardType;
import org.xabgesagtx.bm.events.service.EventService;

import java.util.Optional;
import java.util.regex.Pattern;

import static org.xabgesagtx.bm.bot.common.CommandConstants.DETAILS;
import static org.xabgesagtx.bm.bot.common.CommandConstants.PREFIX;

@Component
@RequiredArgsConstructor
public class EventDetailsChatCommand implements IChatCommand {
	
	private static final Pattern DETAILS_PATTERN = Pattern.compile(PREFIX + DETAILS + "(\\S+)");

	private final EventService facade;
	private final EventDetailsFormatter formatter;

	private Optional<String> getId(String text) {
		var result = Optional.<String>empty();
		var matcher = DETAILS_PATTERN.matcher(text);
		if (matcher.matches()) {
			result = Optional.of(matcher.group(1));
		}
		return result;
	}

	@Override
	public Optional<ChatMessageResponse> toResponse(String text) {
		return getId(text).map(id -> {
			var messageText = createMessageText(id);
			return new ChatMessageResponse(messageText, KeyboardType.MAIN);
		});
	}


	private String createMessageText(String id) {
		return facade.getEvent(id)
				.map(formatter::format)
				.orElse("Sorry, konnte keinen entsprechenden Termin finden");
	}

}
