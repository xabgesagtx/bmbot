package org.xabgesagtx.bm.bot.commands.chat;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;
import org.xabgesagtx.bm.bot.common.CommandConstants;
import org.xabgesagtx.bm.bot.model.responses.ChatMessageResponse;
import org.xabgesagtx.bm.bot.model.responses.KeyboardType;

@Component
public class HelpChatCommand implements IMatchingChatCommand {

	private static final String MESSAGE = "Alle momentan verfügbaren Kommandos sind im Kontextmenü oder auf der Tastatur angezeigt. Dazu kann man ein konkretes Datum eingeben. Z.B. 20.6 für den 20. Juni";

	private String getPrefixedName() {
		return CommandConstants.PREFIX + CommandConstants.HELP;
	}

	@Override
	public boolean matches(String text) {
		return StringUtils.equals(StringUtils.trim(text),getPrefixedName());
	}

	@Override
	public ChatMessageResponse createResponse() {
		return new ChatMessageResponse(MESSAGE, KeyboardType.MAIN);
	}

}
