package org.xabgesagtx.bm.bot.commands.inline;

import org.xabgesagtx.bm.bot.model.responses.InlineArticle;

import java.util.Collections;
import java.util.List;

public interface IMatchingInlineCommand extends IInlineCommand {

	boolean matches(String text);

	List<InlineArticle> getArticles();

	@Override
	default List<InlineArticle> toArticle(String text) {
		if (matches(text)) {
			return getArticles();
		} else {
			return Collections.emptyList();
		}
	}
}
