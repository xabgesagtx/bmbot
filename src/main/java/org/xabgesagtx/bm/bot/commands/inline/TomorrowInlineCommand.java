package org.xabgesagtx.bm.bot.commands.inline;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.xabgesagtx.bm.bot.common.CommandConstants;
import org.xabgesagtx.bm.bot.model.ArticleFactory;
import org.xabgesagtx.bm.bot.model.responses.InlineArticle;
import org.xabgesagtx.bm.events.service.EventService;

import java.util.List;
import java.util.stream.Collectors;

@Component
@RequiredArgsConstructor
public class TomorrowInlineCommand implements IMatchingInlineCommand {

	private final EventService facade;
	private final ArticleFactory articleFactory;

	@Override
	public boolean matches(String text) {
		return CommandConstants.TOMORROW_PATTERN.matcher(text).matches();
	}

	@Override
	public List<InlineArticle> getArticles() {
		return facade.getEventsForTomorrow().stream()
				.map(articleFactory::create)
				.collect(Collectors.toList());
	}

}
