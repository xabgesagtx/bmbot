package org.xabgesagtx.bm.bot.commands.inline;

import org.xabgesagtx.bm.bot.model.responses.InlineArticle;

import java.util.List;

public interface IInlineCommand {

	List<InlineArticle> toArticle(String text);

}
