package org.xabgesagtx.bm.bot.commands.chat;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;
import org.xabgesagtx.bm.bot.common.CommandConstants;
import org.xabgesagtx.bm.bot.model.responses.ChatMessageResponse;
import org.xabgesagtx.bm.bot.model.responses.KeyboardType;

@Component
public class MainMenuChatCommand implements IMatchingChatCommand {

	private static final String MESSAGE = "Triff eine Auswahl oder gib einen Wochentag oder Datum an";

	public boolean matches(String text) {
		return StringUtils.equals(text, CommandConstants.BACK_TEXT);
	}

	@Override
	public ChatMessageResponse createResponse() {
		return new ChatMessageResponse(MESSAGE, KeyboardType.MAIN);
	}

}
