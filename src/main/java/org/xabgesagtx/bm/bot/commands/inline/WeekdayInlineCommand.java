package org.xabgesagtx.bm.bot.commands.inline;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.xabgesagtx.bm.bot.common.DateUtils;
import org.xabgesagtx.bm.bot.model.ArticleFactory;
import org.xabgesagtx.bm.bot.model.responses.InlineArticle;
import org.xabgesagtx.bm.events.service.EventService;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Component
@RequiredArgsConstructor
public class WeekdayInlineCommand implements IInlineCommand {

	private final EventService facade;
	private final ArticleFactory articleFactory;
	private final DateUtils dateUtils;

	@Override
	public List<InlineArticle> toArticle(String text) {
		return dateUtils.getLocalDateFromDayOfWeekString(text)
				.map(date ->  facade.getEventsForDate(date).stream().map(articleFactory::create).collect(Collectors.toList()))
				.orElse(Collections.emptyList());
	}

}

