package org.xabgesagtx.bm.bot.commands.chat;

import org.xabgesagtx.bm.bot.model.responses.ChatMessageResponse;

import java.util.Optional;

public interface IChatCommand {

	Optional<ChatMessageResponse> toResponse(String text);

}
