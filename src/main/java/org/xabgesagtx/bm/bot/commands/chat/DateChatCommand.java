package org.xabgesagtx.bm.bot.commands.chat;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.xabgesagtx.bm.bot.common.DateUtils;
import org.xabgesagtx.bm.bot.formatter.DaySummariesFormatter;
import org.xabgesagtx.bm.bot.model.responses.ChatMessageResponse;
import org.xabgesagtx.bm.bot.model.responses.KeyboardType;
import org.xabgesagtx.bm.events.persistence.model.Event;
import org.xabgesagtx.bm.events.service.EventService;

import java.util.List;
import java.util.Optional;

@Component
@RequiredArgsConstructor
public class DateChatCommand implements IChatCommand {

	private final EventService facade;
	private final DaySummariesFormatter formatter;
	private final DateUtils dateUtils;

	@Override
	public Optional<ChatMessageResponse> toResponse(String text) {
		return dateUtils.getLocalDateFromString(text).map(date -> {
			List<Event> events = facade.getEventsForDate(date);
			String responseText = formatter.format(date, events);
			return new ChatMessageResponse(responseText, KeyboardType.MAIN);
		});
	}

}
