package org.xabgesagtx.bm.bot.formatter;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.xabgesagtx.bm.bot.model.dtos.EventDetailsDTO;
import org.xabgesagtx.bm.events.persistence.model.Event;

import java.util.Collections;
import java.util.Map;

@Component
@RequiredArgsConstructor
@Slf4j
public class EventDetailsFormatter {

	private final FreemarkerWriter writer;

	public String format(Event event) {
		var details = EventDetailsDTO.fromEvent(event);
		Map<String,Object> root = Collections.singletonMap("details", details);
		String result;
		try {
			result = writer.writeAsString(root, "event-details.ftl");
		} catch (FreemarkerException e) {
			log.error("Failed to write event details", e);
			result = "Sorry, leider ist beim Schreiben der Details zu dieser Veranstaltung etwas schiefgegangen";
		}
		return result;
	}

}
