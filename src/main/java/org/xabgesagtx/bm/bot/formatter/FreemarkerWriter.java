package org.xabgesagtx.bm.bot.formatter;

import freemarker.template.Configuration;
import freemarker.template.TemplateException;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.io.StringWriter;
import java.util.Map;

@Component
@RequiredArgsConstructor
public class FreemarkerWriter {

	private final Configuration freemarkerConfig;

	String writeAsString(Map<String,Object> root, String templateName) {
		try {
			var template = freemarkerConfig.getTemplate(templateName);
			var out = new StringWriter();
			template.process(root, out);
			return out.toString();
		} catch (IOException | TemplateException e) {
			throw new FreemarkerException("Could not write string for template " + templateName, e);
		}
	}
}
