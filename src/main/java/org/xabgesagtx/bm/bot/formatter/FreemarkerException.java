package org.xabgesagtx.bm.bot.formatter;

class FreemarkerException extends RuntimeException {

	FreemarkerException(String message, Throwable cause) {
		super(message, cause);
	}

}
