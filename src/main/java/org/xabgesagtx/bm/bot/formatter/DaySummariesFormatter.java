package org.xabgesagtx.bm.bot.formatter;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.stereotype.Component;
import org.xabgesagtx.bm.bot.model.dtos.EventDetailsDTO;
import org.xabgesagtx.bm.events.persistence.model.Event;

import java.time.LocalDate;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static org.xabgesagtx.bm.bot.common.CommandConstants.DATE_FORMATTER;

@Component
@RequiredArgsConstructor
@Slf4j
public class DaySummariesFormatter {

	private final FreemarkerWriter writer;

	public String format(LocalDate day, List<Event> events) {
		return format(Collections.singletonMap(day, events));
	}

	public String format(Map<LocalDate, List<Event>> eventsPerDay) {
		var eventDTOsPerDay = eventsPerDay.entrySet().stream()
				.map(this::convert)
				.collect(Collectors.toMap(Pair::getLeft, Pair::getRight));
		var root = new HashMap<String, Object>();
		List<String> days = eventDTOsPerDay.keySet().stream().sorted().collect(Collectors.toList());
		root.put("days", days);
		root.put("eventsPerDay", eventDTOsPerDay);
		String result;
		try {
			result = writer.writeAsString(root, "days.ftl");
		} catch (FreemarkerException e) {
			log.error("Failed to write day summaries", e);
			result = "Sorry, leider ist beim Schreiben der Tagesübersicht ein Fehler aufgetreten";
		}
		return result;
	}

	private Pair<String, List<EventDetailsDTO>> convert(Map.Entry<LocalDate, List<Event>> entry) {
		var events = entry.getValue().stream()
				.map(EventDetailsDTO::fromEvent)
				.collect(Collectors.toList());
		var dateString = DATE_FORMATTER.format(entry.getKey());
		return Pair.of(dateString, events);
	}
}
