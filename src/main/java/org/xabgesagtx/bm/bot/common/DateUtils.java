package org.xabgesagtx.bm.bot.common;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.time.format.TextStyle;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;
import java.util.stream.Stream;

@Component
public class DateUtils {
	
	private static final int END_OF_DAY_IN_HOURS = 3;
	
	public LocalDate getToday() {
		var result = LocalDateTime.now();
		var limit = LocalDateTime.of(LocalDate.now(), LocalTime.of(3, 0));
		if (result.isBefore(limit)) {
			result = result.minusDays(1);
		}
		return result.toLocalDate();
	}
	
	public LocalDate getTomorrow() {
		return getToday().plusDays(1);
	}

	public LocalDateTime getFullDate(Long seconds, LocalDate date) {
		LocalDateTime result = null;
		if (date != null && seconds != null) {
			LocalDateTime dateTime;
			if (seconds <= TimeUnit.SECONDS.convert(END_OF_DAY_IN_HOURS, TimeUnit.HOURS)) {
				dateTime = date.plusDays(1).atStartOfDay();
			} else {
				dateTime = date.atStartOfDay();
			}
			result = dateTime.plusSeconds(seconds);
		}
		return result;
	}
	
	public List<LocalDate> getWeekend() {
		var now = LocalDate.now();
		return getWeekend(now);
	}
	
	List<LocalDate> getWeekend(LocalDate now) {
		var result = new ArrayList<LocalDate>();
		int dayOfWeek = now.getDayOfWeek().getValue();
		if (dayOfWeek > 5) {
			var toGet = 7 - dayOfWeek;
			result.add(now);
			for(var i = 1; toGet >= i; i++) {
				result.add(now.plusDays(i));
			}
		} else {
			var toAdd = 5 - dayOfWeek;
			result.add(now.plusDays(toAdd));
			result.add(now.plusDays(toAdd + 1));
			result.add(now.plusDays(toAdd + 2));
		}
		return result;
	}
	
	public Optional<LocalDate> getLocalDateFromDayOfWeekString(String text) {
		return getDayOfWeekFromString(text).map(dayOfWeek -> getFittingDateForDayOfWeek(dayOfWeek, getToday()));
	}
	
	LocalDate getFittingDateForDayOfWeek(DayOfWeek dayOfWeek, LocalDate date) {
		LocalDate result;
		var currentDayOfWeek = date.getDayOfWeek();
		var diff = dayOfWeek.getValue() - currentDayOfWeek.getValue();
		if (diff >= 0) {
			result = date.plusDays(diff);
		} else {
			result = date.plusWeeks(1).plusDays(diff);
		}
		return result;
	}
	
	Optional<DayOfWeek> getDayOfWeekFromString(String text) {
		String safeText = StringUtils.defaultString(text);
		return Arrays.stream(DayOfWeek.values())
				.filter(dayOfWeek -> matchesDayOfWeek(dayOfWeek, safeText))
				.findFirst();
	}
	
	private boolean matchesDayOfWeek(DayOfWeek dayOfWeek, String text) {
		return Stream.of(Locale.GERMAN,Locale.US).anyMatch(locale -> matchesDayOfWeek(dayOfWeek, text, locale));
	}
	
	private boolean matchesDayOfWeek(DayOfWeek dayOfWeek, String text, Locale locale) {
		return Stream.of(TextStyle.FULL,TextStyle.SHORT).anyMatch(style -> StringUtils.equalsIgnoreCase(dayOfWeek.getDisplayName(style,locale),text));
	}
	
	private static final DateTimeFormatter FULL_DATE_FORMATTER = DateTimeFormatter.ofPattern("d.M.yyyy");
	private static final DateTimeFormatter FULL_DATE_FORMATTER_WITH_MONTH_NAME = DateTimeFormatter.ofPattern("d.MMMM.yyyy",Locale.GERMAN);
	private static final DateTimeFormatter FULL_DATE_FORMATTER_WITH_MONTH_NAME_US = DateTimeFormatter.ofPattern("d.MMMM.yyyy",Locale.US);
	
	public Optional<LocalDate> getLocalDateFromString(String text) {
		return getLocalDateFromString(text, LocalDate.now());
	}
	
	Optional<LocalDate> getLocalDateFromString(String text, LocalDate now) {
		var appendedYear = false;
		if (Pattern.matches("^(\\d{1,2})\\.\\s?(\\d{1,2}\\.?)|(\\w+)$",text)) {
			if (!StringUtils.endsWith(text, ".")) {
				text = text + ".";
			}
			appendedYear = true;
			text = text + now.getYear();
		}
		var searchText = text.replaceAll("\\s+", StringUtils.EMPTY);
		var result = Stream.of(FULL_DATE_FORMATTER, FULL_DATE_FORMATTER_WITH_MONTH_NAME, FULL_DATE_FORMATTER_WITH_MONTH_NAME_US).flatMap(formatter -> parseSafely(formatter, searchText)).findFirst();
		if (appendedYear && result.isPresent() && result.get().plusMonths(1).isBefore(now)) {
			result = Optional.of(result.get().plusYears(1));
		}
		return result;
	}
	
	private Stream<LocalDate> parseSafely(DateTimeFormatter formatter, String text) {
		var result = Stream.<LocalDate>empty();
		try {
			result = Stream.of(LocalDate.parse(text, formatter));
		} catch (DateTimeParseException e) {
			// nothing to do
		}
		return result;
	}
}
