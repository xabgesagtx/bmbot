package org.xabgesagtx.bm.bot.common;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import java.time.format.DateTimeFormatter;
import java.util.Locale;
import java.util.regex.Pattern;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class CommandConstants {

	public static final DateTimeFormatter DATE_FORMATTER = DateTimeFormatter.ofPattern("EEEE, d.M.").withLocale(Locale.GERMAN);

	public static final String PREFIX = "/";
	public static final String START = "start";
	public static final String DETAILS = "e";
	public static final String HELP = "help";
	public static final String BACK_TEXT = "Zurück";
	public static final String WEEKDAYS_TEXT = "Wochentage";

	public static final Pattern TODAY_PATTERN = Pattern.compile("^(heute|today|jetzt|gerade)$", Pattern.CASE_INSENSITIVE);
	public static final Pattern TOMORROW_PATTERN = Pattern.compile("^(tomorr?ow|morgen)$", Pattern.CASE_INSENSITIVE);
	public static final Pattern WEEKEND_PATTERN = Pattern.compile("^(wochenende|weekend)$", Pattern.CASE_INSENSITIVE);
}
