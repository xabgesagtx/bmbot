package org.xabgesagtx.bm.metrics;

import lombok.RequiredArgsConstructor;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.inlinequery.InlineQuery;
import org.xabgesagtx.bm.bot.model.responses.ChatMessageResponse;
import org.xabgesagtx.bm.bot.model.responses.InlineArticle;

import java.util.List;
import java.util.Optional;

@Aspect
@Component
@RequiredArgsConstructor
public class UsageAspect {

	private final BmMetrics metrics;

	@AfterReturning(value = "execution(* org.xabgesagtx.bm.bot.commands.chat.IChatCommand.toResponse(..))", returning = "responseOpt")
	public void chatMessagesHandled(JoinPoint jp, Optional<ChatMessageResponse> responseOpt) {
		if (responseOpt.isPresent()) {
			var clazz = jp.getTarget().getClass();
			metrics.incMessagesHandled(clazz);
		}
	}

	@AfterReturning(value = "execution(* org.xabgesagtx.bm.bot.commands.inline.IInlineCommand.toArticle(..))", returning = "articles")
	public void inlineQueryHandled(JoinPoint jp, List<InlineArticle> articles) {
		if (!articles.isEmpty()) {
			var clazz = jp.getTarget().getClass();
			metrics.incQueriesHandled(clazz);
		}
	}

	@Before(value = "execution(* org.xabgesagtx.bm.bot.handler.ChatMessageHandler.handle(..)) && args(message)", argNames = "message")
	public void chatMessageIncoming(Message message) {
		metrics.incMessagesIncoming();
		metrics.incMessagesIncomingChat(message.getChatId());
	}

	@Before(value = "execution(* org.xabgesagtx.bm.bot.handler.InlineQueryHandler.handle(..)) && args(query)", argNames = "query")
	public void inlineQueriesIncoming(InlineQuery query) {
		metrics.incQueriesIncoming();
	}



}
