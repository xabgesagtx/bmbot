package org.xabgesagtx.bm.metrics;

import lombok.RequiredArgsConstructor;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;

@Aspect
@Component
@RequiredArgsConstructor
public class PersistenceAspect {

	private final BmMetrics metrics;

	@AfterThrowing(value = "execution(* org.xabgesagtx.bm.events.service.EventService.replaceAll(..))", throwing = "throwable")
	public void replaceAll(Throwable throwable) {
		metrics.incPersistenceError();
		metrics.incPersistenceErrorClass(throwable.getClass());
	}


}
