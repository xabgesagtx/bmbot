package org.xabgesagtx.bm.metrics;

import io.micrometer.core.instrument.Counter;
import io.micrometer.core.instrument.MeterRegistry;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.util.ClassUtils;
import org.xabgesagtx.bm.bot.commands.chat.IChatCommand;
import org.xabgesagtx.bm.bot.commands.inline.IInlineCommand;

import javax.annotation.PostConstruct;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

@Component
@RequiredArgsConstructor
public class BmMetrics {

	private static final String METRIC_PERSISTENCE_ERROR = "persistence.error";
	private static final String METRIC_PERSISTENCE_ERROR_CLASS = "persistence.error.class";
	private static final String METRIC_SCRAPER_EVENTS = "scraper.events";
	private static final String METRIC_SCRAPER_ERROR = "scraper.error";
	private static final String METRIC_SCRAPER_ERROR_CLASS = "scraper.error.class";
	private static final String METRIC_MESSAGES_HANDLED = "messages.handled";
	private static final String METRIC_QUERIES_HANDLED = "queries.handled";
	private static final String METRIC_MESSAGES_INCOMING = "messages.incoming";
	private static final String METRIC_MESSAGES_INCOMING_CHAT = "messages.incoming.chat";
	private static final String METRIC_QUERIES_INCOMING = "queries.incoming";

	private static final String TAG_CLASS = "class";
	private static final String TAG_COMMAND = "command";

	private static final String TAG_CHAT_ID = "chatId";

	private final MeterRegistry registry;
	private final List<IChatCommand> chatCommands;
	private final List<IInlineCommand> inlineCommands;
	private final AtomicInteger scrapedEvents = new AtomicInteger(0);

	void incMessagesHandled(Class<?> clazz) {
		messagesHandled(clazz).increment();
	}

	void incQueriesHandled(Class<?> clazz) {
		queriesHandled(clazz).increment();
	}

	void incMessagesIncoming() {
		messagesIncoming().increment();
	}

	void incMessagesIncomingChat(long chatId) {
		registry.counter(METRIC_MESSAGES_INCOMING_CHAT, TAG_CHAT_ID, String.valueOf(chatId)).increment();
	}

	void incPersistenceError() {
		persistenceError().increment();
	}

	void incPersistenceErrorClass(Class<?> clazz) {
		registry.counter(METRIC_PERSISTENCE_ERROR_CLASS, TAG_CLASS, clazz.getSimpleName()).increment();
	}

	void incQueriesIncoming() {
		queriesIncoming().increment();
	}

	void incScraperError() {
		scraperError().increment();
	}

	void incScraperErrorClass(Class<?> clazz) {
		registry.counter(METRIC_SCRAPER_ERROR_CLASS, TAG_CLASS, clazz.getSimpleName()).increment();
	}

	void scrapedEvents(int numberOfEvents) {
		scrapedEvents.set(numberOfEvents);
	}

	private Counter messagesHandled(Class<?> clazz) {
		return registry.counter(METRIC_MESSAGES_HANDLED, TAG_COMMAND, clazz.getSimpleName());
	}

	private Counter messagesIncoming() {
		return registry.counter(METRIC_MESSAGES_INCOMING);
	}

	private Counter persistenceError() {
		return registry.counter(METRIC_PERSISTENCE_ERROR);
	}

	private Counter queriesHandled(Class<?> clazz) {
		return registry.counter(METRIC_QUERIES_HANDLED, TAG_COMMAND, clazz.getSimpleName());
	}

	private Counter queriesIncoming() {
		return registry.counter(METRIC_QUERIES_INCOMING);
	}

	private Counter scraperError() {
		return registry.counter(METRIC_SCRAPER_ERROR);
	}

	@PostConstruct
	public void initCounters() {
		registry.gauge(METRIC_SCRAPER_EVENTS, scrapedEvents);
		messagesIncoming();
		queriesIncoming();
		chatCommands.stream()
				.map(ClassUtils::getUserClass)
				.forEach(this::messagesHandled);
		inlineCommands.stream()
				.map(ClassUtils::getUserClass)
				.forEach(this::queriesHandled);
		persistenceError();
		scraperError();
	}

}
