package org.xabgesagtx.bm.metrics;

import lombok.RequiredArgsConstructor;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;
import org.xabgesagtx.bm.events.persistence.model.Event;

import java.util.List;

@Aspect
@Component
@RequiredArgsConstructor
public class ScrapingAspect {

	private final BmMetrics metrics;

	@AfterReturning(value = "execution(* org.xabgesagtx.bm.events.service.BmScraper.getEvents())", returning = "events")
	public void scraping(List<Event> events) {
		metrics.scrapedEvents(events.size());
	}

	@AfterThrowing(value = "execution(* org.xabgesagtx.bm.events.service.BmScraper.getEvents())", throwing = "throwable")
	public void scraping(Throwable throwable) {
		metrics.scrapedEvents(0);
		metrics.incScraperError();
		metrics.incScraperErrorClass(throwable.getClass());
	}
}
