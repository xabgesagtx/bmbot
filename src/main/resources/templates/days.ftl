<#list days as day>
<b>${day}</b>

<#if eventsPerDay[day]?has_content>
<#list eventsPerDay[day] as event>
<#if event.canceled><b>Abgesagt</b> </#if><#if event.startTime?has_content>${event.startTime} </#if>${event.locationTitle}
<b>${event.title}</b> /e${event.cleanId}

</#list>
<#else>
Leider haben wir keine Veranstaltungen für diesen Termin.
</#if>
</#list>