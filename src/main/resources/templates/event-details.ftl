${details.date}

<b>${details.title}</b> <#if details.canceled><b>(Abgesagt)</b></#if>
<#if details.startTime?has_content>${details.startTime} </#if>${details.locationTitle}
<#if details.price?has_content>
Preis: ${details.price}
</#if>
<#if details.description?has_content>

${details.description}
</#if>
<#if details.links?has_content>

<b>Links:</b>
<#list details.links as link>
<a href="${link.url}">${link.title}</a>
</#list>
</#if>
<#if details.bands?has_content>

<b>Bands:</b>
<#list details.bands as band>
${band.title} <#if band.description?has_content>- ${band.description}</#if>
<#list band.links as link><a href="${link.url}">${link.title}</a> </#list>
</#list>
</#if>